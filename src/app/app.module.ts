import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { New } from '../pages/new/new';
import { Detail } from '../pages/detail/detail';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    New,
    Detail
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    New,
    Detail
  ],
  providers: []
})
export class AppModule {}
