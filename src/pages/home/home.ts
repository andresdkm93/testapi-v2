import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {New} from '../new/new';
import {Detail} from '../detail/detail';
import {PostService} from '../../providers/post-service';
import {PostModel} from "../../../.tmp/models/post";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PostService]
})
export class HomePage {
  posts: any;
  constructor(public navCtrl: NavController, private postsService: PostService) {
    this.postsService.get()
        .then(data => {
          this.posts = data;
        });
  };

  goToNew(){
    this.navCtrl.push(New);
  }

  goToDetail(id:number)
  {
    this.navCtrl.push(Detail,{post_id:id});
  }


}
