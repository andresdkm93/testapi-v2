import { Component } from '@angular/core';
import { NavController,LoadingController  } from 'ionic-angular';
import {PostService} from '../../providers/post-service';
import {PostModel} from '../../models/post';
import {HomePage} from '../home/home';

/*
  Generated class for the New page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-new',
  templateUrl: 'new.html',
  providers: [PostService]

})
export class New {
  post:any;
  constructor(public navCtrl: NavController,private postsService: PostService,public loadingCtrl: LoadingController) {
    this.post=new PostModel();
  }

  save(){
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();

    this.postsService.save(new PostModel(this.post))
      .then(data => {
        loader.dismissAll();
        this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: "forward"});
      });

  }

}
