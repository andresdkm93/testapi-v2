import { Component } from '@angular/core';
import { NavController,NavParams,LoadingController } from 'ionic-angular';
import {PostModel} from '../../models/post';
import {PostService} from '../../providers/post-service';
import {HomePage} from '../home/home';


/*
  Generated class for the Detail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
  providers: [PostService]

})
export class Detail {
  id:number;
  post:any;
  constructor(public navCtrl: NavController,public params: NavParams, private postsService: PostService,public loadingCtrl: LoadingController) {
    this.post=new PostModel();
    this.id=this.params.get('post_id');
    this.postsService.get(this.id)
      .then(data => {
        this.post = data;
      });
  }
  update()
  {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.postsService.update(this.post,this.id)
      .then(data => {
        loader.dismissAll();
        this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: "forward"});
      });

  }
  delete()
  {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.postsService.delete(this.id)
      .then(data => {
        loader.dismissAll();
        this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: "forward"});
      });

  }



}
