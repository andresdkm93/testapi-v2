import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions,Response} from '@angular/http';
import {PostModel} from '../models/post'
import {AppSettings} from '../utils/app.settings';
import 'rxjs/add/operator/map';

/*
 Generated class for the PostService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class PostService {

  constructor(private http:Http) {
  }

  get(id?:number) {
    return new Promise(resolve => {
      if(id){
        this.http.get(`${AppSettings.API_ENDPOINT}posts` +  "/"+id)
          .map(res => res.json())
          .subscribe(data=> {
            resolve(new PostModel(data));
          });
      }else{
        this.http.get(`${AppSettings.API_ENDPOINT}posts`)
          .map(res => res.json())
          .subscribe((posts:Object[]) => {
            var data = posts.map(post => this.parseData(post));
            resolve(data);
          });
      }

    });
  }

  save(post:PostModel) {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return new Promise(resolve => {
      this.http.post(`${AppSettings.API_ENDPOINT}posts`,post,options)
        .map((res:Response) => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  update(post:PostModel,id:number) {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return new Promise(resolve => {
      this.http.put(`${AppSettings.API_ENDPOINT}posts/`+id,post,options)
        .map((res:Response) => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  delete(id:number) {
  return new Promise(resolve => {
    this.http.delete(`${AppSettings.API_ENDPOINT}posts/`+id)
      .map((res:Response) => res.json())
      .subscribe(data => {
        resolve(data);
      });
  });
}

  private parseData(data):PostModel {
    return new PostModel(data);
  }

}
