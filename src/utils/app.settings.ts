/**
 * Created by AndresFabian on 6/10/2016.
 */
export class AppSettings {
  public static get API_ENDPOINT():string {
    return 'https://jsonplaceholder.typicode.com/';
  }
}
