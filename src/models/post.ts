/**
 * Created by AndresFabian on 6/10/2016.
 */
export class PostModel {
  title:string;
  body:string;
  userId:number;
  id:number;

  constructor(post?:any) {
    this.title = post?post.title:"";
    this.body = post? post.body:"";
    this.userId = post?post.userId:null;
    this.id = post ? post.id : null;
  }

}
